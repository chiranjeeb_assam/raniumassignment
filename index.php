<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="datepicker/datepicker3.css">
	<style type="text/css">
	.neo{
		text-align: center;
	}
</style>
</head>
<body>

	<section style="margin-top:60px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<form class="form-horizontal" id="myform">
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Start date:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control datepicker" id="startdate" placeholder="start date">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="pwd">End date:</label>
							<div class="col-sm-10"> 
								<input type="text" class="form-control datepicker" id="enddate" placeholder="End date">
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<section class="chartsection">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="graph" id="example"></div>
				</div>
			</div>
		</div>
	</section>

	<section style="margin-top: 40px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<strong>Total number of asteroids:</strong>  <span id="totaster"></span>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h3>Near earth objects</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="neo">
						
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<script type="text/javascript" src="jquery/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="graphite/graphite.js"></script>
	<script>

		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true
		});

		$("#myform").on('submit', function(e){
			e.preventDefault();
			
			$(".graph").empty();
			$("#totaster").empty();
			$(".neo").empty();
			

			var startdate = $("#startdate").val();
			var enddate = $("#enddate").val();
			
			var url = "https://api.nasa.gov/neo/rest/v1/feed?start_date="+startdate+"&end_date="+enddate+"&api_key=zOL0q6GgPkorRvRS2RBv3XOaRqT097BzVTysMB0v";

			$.ajax({
				url: url,
				success: function(result){
					
					var exampleData = new Array();

					$("#totaster").text(result.element_count);

					$.each(result.near_earth_objects, function( index, value ) {
						//console.log(index, value);
						var count = 0;
						
						field = '<h2>Date:'+index+'</h2>';
						$(".neo").append(field);
						$.each(value, function(index, value){
							
							field2 = '<p>Name: '+value.name+'</p>';
							$(".neo").append(field2);
							count++;
						})

						exampleData.push(count);
						console.log(exampleData);

						count = 0;
					});

					var exampleOptions = {
						'height': 350,
						'title': 'Number of Objects',
						'width': 20,
						'fixPadding': 18,
						'barFont': [0, 12, "bold"],
						'labelFont': [0, 13, 0]
					};

					graphite(exampleData, exampleOptions, example);
					
				},
				error: function (error) {
				    alert(error.responseJSON.error_message);
				}
			});

			


		});

	</script>
</body>
</html>